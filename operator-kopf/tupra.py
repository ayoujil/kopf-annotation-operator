import kopf
from kubernetes import client, config

# Charger la configuration Kubernetes hors cluster
config.load_kube_config()

# Créer un objet ApiClient
api_client = client.ApiClient()
v1 = client.CoreV1Api(api_client)

############################
# si la CR est déployée, l'annotation est ajoutée au namespace
############################
@kopf.on.field('tupra.edf.fr', field='spec.site')
@kopf.on.create('tupra.edf.fr')
def add_site_annotation(body, **kwargs):

    site = body['spec']['site']
    tu_pra_namespace_name = body['metadata']['namespace']

    if site not in ["pacy", "noe"]:
        raise kopf.TemporaryError("Nom de site inexistant .. Merci de choisir entre Pacy et Noe !!", delay=30)

    edf_site = "edf.fr/site=" + site
    tu_pra_namespace_annotations = {"openshift.io/node-selector": edf_site}

    tu_pra_namespace_object = v1.read_namespace(tu_pra_namespace_name)
    annotations = tu_pra_namespace_object.metadata.annotations
    if annotations:
        annotations.update(tu_pra_namespace_annotations)
    else:
        annotations = tu_pra_namespace_annotations
    tu_pra_namespace_object.metadata.annotations = annotations
    v1.patch_namespace(tu_pra_namespace_name, tu_pra_namespace_object)

############################
# Si la CR est supprimée, l'annotation est supprimé du NS
############################
@kopf.on.delete('tupra.edf.fr')
def delete_site_annotation(body, **kwargs):

    site = body['spec']['site']
    tu_pra_namespace_name = body['metadata']['namespace']
    
    tu_pra_namespace_object = v1.read_namespace(tu_pra_namespace_name)
    annotations = tu_pra_namespace_object.metadata.annotations

    if "openshift.io/node-selector" in annotations:
        del annotations["openshift.io/node-selector"]
        tu_pra_namespace_object.metadata.annotations = annotations
        v1.replace_namespace(tu_pra_namespace_name, tu_pra_namespace_object)

############################
# Si la CR change, l'annotation change avec condition
# Condition1 : site est différent de pacy ou noe => message d'erreur ne continue pas
# Condition2 : site pacy ou noe => changer la valeur de l'annotation
############################
#@kopf.on.update('tupra.edf.fr')
#
#    site = body['spec']['site']
#    tu_pra_namespace_name = body['metadata']['namespace']
#
#    if site not in ["pacy", "noe"]:
#        raise kopf.TemporaryError("Nom de site inexistant .. Merci de choisir entre Pacy et Noe !!", delay=30)
#
#    edf_site = "edf.fr/site=" + site
#    tu_pra_namespace_annotations = {"openshift.io/node-selector": edf_site}
#
#    tu_pra_namespace_object = v1.read_namespace(tu_pra_namespace_name)
#    annotations = tu_pra_namespace_object.metadata.annotations
#    if annotations:
#        annotations.update(tu_pra_namespace_annotations)
#    else:
#        annotations = tu_pra_namespace_annotations
#    tu_pra_namespace_object.metadata.annotations = annotations
#    v1.patch_namespace(tu_pra_namespace_name, tu_pra_namespace_object)


# Exécuter l'opérateur avec kopf
if __name__ == '__main__':
    kopf.run()
